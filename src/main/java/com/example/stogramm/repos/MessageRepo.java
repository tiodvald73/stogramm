package com.example.stogramm.repos;

import com.example.stogramm.domain.Message;
import org.springframework.data.repository.CrudRepository;

public abstract class MessageRepo implements CrudRepository<Message, Long> {
}
